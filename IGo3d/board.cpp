#include "board.h"

Board::Board(QObject *parent, Ogre::SceneManager  * sceneMgr) : QObject( parent)
{
    mSceneMgr = sceneMgr;
    size = settings.value("board/defualtSize", 19).toInt();
    mState = Playing;
    currentMoveType = false;
    boardMesh = mSceneMgr->createEntity("board", "board.mesh");
    boardNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
    boardNode->attachObject(boardMesh);
    createStones();
    moves = new QTreeWidgetItem((QTreeWidget*)0, QStringList("Root"));
    boardNode->translate(Ogre::Vector3(0,0,-25));
    boardNode->scale(10,10,10);
}

void Board::createStones()
{
    for(int x=1; x <= size; x++)
    {
        for(int y=1; y <= size; y++)
        {
            char buffer[33];
            Ogre::String name("Stone ");
            sprintf(buffer,"%d",20 - y);
            if(x >= 9)
            {
             name.append(QString( x+65 ).toUtf8().constData());
            }
            else
            {
            name.append(QString( x+64 ).toUtf8().constData());
            }
            name.append(buffer);
            activeStoneMesh = mSceneMgr->createEntity(name,"stone.mesh");
            activeStoneNode = boardNode->createChildSceneNode( name, Ogre::Vector3( x * 0.52 - 5.2, 0.3, y * 0.52 - 5.2 ) );
            activeStoneNode->attachObject(activeStoneMesh);
            activeStoneNode->scale(0.25,0.25,0.25); //To be applied to the mesh itself int the future
            activeStoneNode->setVisible(false);
        }
    }
}

void Board::addStone( QString &name , bool ignoreState )
{
   if (mState == Playing || ignoreState == true )
   {
    Ogre::String mName = name.toUtf8().constData();
    activeStoneNode = mSceneMgr->getSceneNode(mName);
    activeStoneMesh = mSceneMgr->getEntity(mName);
    if (!activeStoneMesh->getVisible())
    {
    activeStoneNode->setVisible(true);
    if( currentMoveType == true){
        activeStoneMesh->setMaterialName("White_Stone");
        currentMoveType = false;
    }
    else if( currentMoveType == false){
        activeStoneMesh->setMaterialName("Black_Stone");
        currentMoveType = true;
    }
    moves->addChild(new QTreeWidgetItem(moves,QStringList(name)));
    emit changedMoveTree(moves);
    QSound::play("../media/sounds/Go_Stone_Place.wav");
    }
   }
}

void Board::opponentAddStone( QString &name )
{
}

void Board::removeStone( QString &stone )
{
    stone.prepend("Stone ");
    Ogre::String mName = stone.toUtf8().constData();
    activeStoneNode = mSceneMgr->getSceneNode(mName);
    activeStoneMesh = mSceneMgr->getEntity(mName);
    activeStoneNode->setVisible(false);
    stone.prepend("Captured");
    stone.remove("Stone");
    moves->addChild(new QTreeWidgetItem(moves,QStringList(stone)));
}

void Board::addToMoveTree( QTreeWidgetItem *add )
{
    QString name(add->text(0));
    name.prepend("Stone ");
    addStone(name, true);
}

void Board::setState(int State)
{
if (State == 1)
    mState = Playing;
if (State == 2)
    mState = Observing;
}

void Board::clearStones( )
{
    for(int x=1; x <= size; x++)
    {
        for(int y=1; y <= size; y++)
        {
            char buffer[33];
            Ogre::String name("Stone ");
            sprintf(buffer,"%d",20 - y);
            if(x >= 9)
            {
             name.append(QString( x+65 ).toUtf8().constData());
            }
            else
            {
            name.append(QString( x+64 ).toUtf8().constData());
            }
            name.append(buffer);
            activeStoneNode = mSceneMgr->getSceneNode(name);
            activeStoneNode->setVisible(false);
        }
    }
    currentMoveType = false;
    mState = Playing;
    emit clearMoveTree();
}
