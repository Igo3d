#ifndef ROOM_H
#define ROOM_H

#include <QSound>
#include <Ogre.h>

class Room
{
public:
    Room( Ogre::SceneManager  * sceneMgr );

private:
    Ogre::SceneManager  *mSceneMgr;

    Ogre::SceneNode *roomNode;
    Ogre::Entity    *roomMesh;
    Ogre::SceneNode *chairNode1; //Chairs not used yet.
    Ogre::Entity    *chairmesh1;
    Ogre::SceneNode *chairNode2;
    Ogre::Entity    *chairmesh2;
};

#endif // ROOM_H
