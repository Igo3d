#ifndef BOARD_H
#define BOARD_H

#include <QtCore>
#include <QtGui>
#include <Ogre.h>

class Board : public QObject
{
   Q_OBJECT

public:
    Board( QObject *parent = 0, Ogre::SceneManager  * sceneMgr = 0 );

    enum State
    {
        Playing,
        Observing
    };

    void addStone( QString &name, bool ignoreState = false );

public slots:
    void addToMoveTree( QTreeWidgetItem *add );
    void opponentAddStone( QString &name );
    void removeStone( QString &stone );
    void setState( int State );
    void clearStones();

signals:
    void changedMoveTree( QTreeWidgetItem *change );
    void clearMoveTree();


public:
    State mState;
private:
    QSettings settings;

    int size;
    int currentMoveNum;
    bool currentMoveType;

    QTreeWidgetItem *moves;

    Ogre::SceneManager *mSceneMgr;

    Ogre::Entity    *boardMesh;
    Ogre::SceneNode *boardNode;
    Ogre::Entity    *activeStoneMesh;
    Ogre::SceneNode *activeStoneNode;

private:
    void createStones();
};

#endif // BOARD_H
