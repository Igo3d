
#include <QTextCodec>
#include <QTextStream>
#include "telnet.h"

telnet::telnet( QObject *parent ) : QtTelnet (parent)
{
    parsingStatus = Ready;
}

void telnet::init(QByteArray name, QByteArray pass, QString host, quint16 port)
{
    this->connectToHost(host, port);
    this->socket()->write(name);
    this->socket()->write("\r");
    this->socket()->write(pass);
    this->socket()->write("\r");
}

void telnet::sendToHost(QString txt)
{

    QTextCodec* textCodec = QTextCodec::codecForName("Shift-JIS"); //Set the text codec to Shift-JIS because IGS doesn't recognize any other codec
    QByteArray data = textCodec->fromUnicode(txt);
    this->socket()->write(data);//Send the text directly through the socket QtTelnet::snedData isn't used becuase it  strips all codecs
    this->socket()->write("\r");//add an end line
}

QString telnet::parse(const QString &msg)
{
    QString nmsg;
    QTextStream stream(&QString(msg));
    QString line;
    QString tmp;
    QStringList list;
    nmsg.remove("��");
    do {
        line = stream.readLine();
        tmp = line;

        if (line.contains(QRegExp("^9 ")))
        {
            nmsg.append(line);
            nmsg.remove("9 ");
            if (line.contains("File"))
            {
            nmsg.remove("File");
            if (parsingStatus == Commenting) parsingStatus = Ready;
            if (parsingStatus == Ready) parsingStatus = Commenting;
            }
        }

        else if (line.contains("15 "))
        {

            if (line.contains(QRegExp("[0-9]{1,5}[(][BW][)]: [A-Z][0-9]{1,2}")))
            {
                QRegExp rx("([A-Z][0-9]{1,10})");
                list.clear();
                int pos = 0;
                while ((pos = rx.indexIn(tmp, pos)) != -1) {
                    list << rx.cap(1);
                    pos += rx.matchedLength();
                }
                emit addToMoveTree(new QTreeWidgetItem((QTreeWidget*)0, QStringList(list.first())));
                list.removeFirst();
                emit refreshOGRE();
                while (!list.isEmpty())
                {
                    emit captureStone(list.first());
                    list.removeFirst();
                }

                //nmsg.remove(line.remove("15 "));
            }
        }

        else if (line.contains(QRegExp("^7")) && line != "7 ")
        {
            tmp.remove(QRegExp("^7"));
            tmp.remove("[");
            tmp.remove("]");
            tmp.remove("(");
            tmp.remove(")");
            tmp.remove("vs.");
            list.append(tmp.split(QRegExp("\\s+"),QString::SkipEmptyParts));
            if (list.size() == 12)
            {
                emit addToGameTree(new QTreeWidgetItem((QTreeWidget*)0, list));
                parsingStatus = Ready;
                list.clear();
            }
            //nmsg.remove(line);
        }

        else
        {
        nmsg.append(line);
        nmsg.append("\n");
        }

    } while (!line.isNull());
    return nmsg;
}
