#ifndef TELNET_H
#define TELNET_H

#include <QtTelnet>
#include <QTreeWidgetItem>

class telnet : public QtTelnet
{
    Q_OBJECT
public:
    telnet( QObject *parent = 0 );


    enum status
    {
        Ready,
        Commenting,
        Moves,
        Observing
    };

public: //I like to keep things seperated
    void init(QByteArray name, QByteArray pass, QString host = QString("210.155.158.200"), quint16 port = 7777);
    void sendToHost(QString txt);
    QString parse(const QString &msg);
    status parsingStatus;

private:
    QString message;

signals:
    void addToMoveTree(QTreeWidgetItem *add);
    void addToGameTree(QTreeWidgetItem *add);
    void setBoardState( int State );
    void captureStone( QString &stone );
    void refreshOGRE();
};

#endif // TELNET_H
