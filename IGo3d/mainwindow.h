#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "telnet.h"
#include "ogrewidget.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    OgreWidget *ogreWidget;
    telnet *t;
    QString parse(const QString &msg);
private slots:
    void telnetMessage( const QString &msg );
    void telnetConnect();
    void telnetDisConnect();
    void on_actionConnect_triggered();
    void onChangedMoveTree(QTreeWidgetItem* changed);
    void onOgreInitComplete();
    void addToGameTree(QTreeWidgetItem *add);
};

#endif // MAINWINDOW_H
