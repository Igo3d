include(.\qttelnet-2.1_1\src\qttelnet.pri)
QT += svg network
TARGET = IGo3d
unix { 
    # You may need to change this include directory
    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include
    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}
win32 {
# You may need to change this include directory
    OGREHOME = C:\Qt\2010.04\OgreSDK_mingw_v1-7-1
    LIBS += -L$$OGREHOME\boost_1_42\lib
    Release:LIBS += -L$$OGREHOME\lib\release
    Debug:LIBS += -L$$OGREHOME\lib\debug
    INCLUDEPATH += $$OGREHOME\include\OGRE
    INCLUDEPATH += $$OGREHOME\boost_1_42
}
CONFIG(debug, debug|release) { 
    TARGET = $$join(TARGET,,,d)
    LIBS *= -lOgreMain_d
    LIBS *= -llibboost_thread-mgw44-mt-d-1_42
    LIBS *= -llibboost_date_time-mgw44-mt-d-1_42
}
CONFIG(release, debug|release){
    LIBS *= -lOgreMain
    LIBS *= -llibboost_thread-mgw44-mt-1_42
    LIBS *= -llibboost_date_time-mgw44-mt-1_42
}
SOURCES += main.cpp \
    mainwindow.cpp \
    ogrewidget.cpp \
    telnet.cpp \
    preferences.cpp \
    room.cpp \
    board.cpp
HEADERS += mainwindow.h \
    ogrewidget.h \
    telnet.h \
    preferences.h \
    room.h \
    board.h
FORMS += mainwindow.ui \
    preferences.ui

TRANSLATIONS = IGo3d_fr.ts \
IGo3d_ro.ts

RESOURCES += \
    Igo3d.qrc
