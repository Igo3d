#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ogreWidget = ui->ogreWidget;

    connect(ogreWidget, SIGNAL(initComplete()), this, SLOT(onOgreInitComplete()));

    tabifyDockWidget(ui->dockGames, ui->dockInGame);
    tabifyDockWidget(ui->dockInGame, ui->dockConnection);
    ui->dockConnection->activateWindow();
    t = new telnet;

    connect(t, SIGNAL(message(QString)),
            this, SLOT(telnetMessage(QString)));
    connect(t, SIGNAL(refreshOGRE()),
            ogreWidget,SLOT(update()));
    connect(t->socket(), SIGNAL(disconnected()),
            this, SLOT(telnetDisConnect()));
    connect(t->socket(), SIGNAL(connected()),
            this, SLOT(telnetConnect()));

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::telnetMessage( const QString &msg ){
    ui->textBrowser->append(t->parse(msg));
}
void MainWindow::telnetDisConnect()
{
    if (ui->connectButton->isChecked()==true)ui->connectButton->setChecked(false);
    if (ui->actionConnect->isChecked()==true)ui->actionConnect->setChecked(false);
    ogreWidget->ogreBoard->clearStones();
    ogreWidget->update();
}

void MainWindow::telnetConnect()
{
    if (ui->connectButton->isChecked()==false)ui->connectButton->setChecked(true);
    if (ui->actionConnect->isChecked()==false)ui->actionConnect->setChecked(true);
}

void MainWindow::on_actionConnect_triggered()
{
    if(ui->actionConnect->isChecked() == true)
    {
    t->init(QByteArray("guest"), QByteArray(""),QString("igs.joyjoy.net"));
    t->sendToHost("toggle client true");
    t->sendToHost("moves 22");
    t->sendToHost("games");
    ogreWidget->ogreBoard->setState(2);
    }
    else
    {
        ui->textBrowser->clear();
        t->close();
    }
}

void MainWindow::onChangedMoveTree(QTreeWidgetItem* changed)
{
ui->treeWidget_Moves->addTopLevelItem(changed);
}

void MainWindow::onOgreInitComplete()
{
connect(ogreWidget->ogreBoard, SIGNAL(changedMoveTree(QTreeWidgetItem*)), this, SLOT(onChangedMoveTree(QTreeWidgetItem*)));
connect(ogreWidget, SIGNAL(newFPS(double)),ui->fpsCount,SLOT(display(double)));
connect(t, SIGNAL(addToMoveTree(QTreeWidgetItem*)), ogreWidget->ogreBoard, SLOT(addToMoveTree(QTreeWidgetItem*)));
connect(t, SIGNAL(setBoardState(int)), ogreWidget->ogreBoard, SLOT(setState(int)));
connect(t, SIGNAL(captureStone(QString&)), ogreWidget->ogreBoard, SLOT(removeStone(QString&)));
connect(t,SIGNAL(addToGameTree(QTreeWidgetItem*)),this, SLOT(addToGameTree(QTreeWidgetItem*)));
}
void MainWindow::addToGameTree(QTreeWidgetItem *add)
{
ui->treeWidget->addTopLevelItem(add);
}
